<?php
    class Htmalpage {
       protected $title = '[title goes here]';
       protected $body = '[body goes here]';
       public function view(){
        echo "<html>
                <head>
                <title> $this->title</title>
                </head>
                <body>
                <p style = 'color:$this->color'>$this->body</p>
                </body>
            </html>";
       }
       function __construct($title = "", $body = ""){ 
            if($title != ""){
                $this->title = $title;
            }
            if($body != ""){
                $this->body = $body;
            }
       }
    }

    class coloredHtmal extends Htmalpage {
        protected $color = 'red';
        function __construct($title = "", $body = ""){ 
            parent::__construct($title, $body);
        }
        public function __set($property,$value){
            if($property == 'color'){ 
                $colors = array('blue','yellow','green','black','pink','gray','gold');
                if(in_array($value,$colors)){ 
                    $this->color = $value; 
                }
                else {
                    die("Please select one of the allowed colors");
                }
            }
        }
        public function view(){
            echo "<html>
                    <head>
                    <title> $this->title</title>
                    </head>
                    <body>
                    <p style = 'color:$this->color'>$this->body</p>
                    </body>
                </html>";
           }
    }

    class fontSizeHtmal extends coloredHtmal {
        protected $fontsize = '10';
        function __construct($title = "", $body = ""){ 
            parent::__construct($title, $body);
        }
        public function __set($property,$value){
            parent::__set($property,$value);
            if($property == 'fontsize'){ 
                $fontsizes = array('10','11','12','13','14','15','16','17','18','19','20','21','22','23','24');
                if(in_array($value,$fontsizes)){ 
                    $this->fontsize = $value; 
                }
                else {
                    die("Please select one of the allowed font size");
                }
            }
        }
        public function view(){
            echo "<html>
                    <head>
                    <title> $this->title</title>
                    </head>
                    <body>
                    <p style = 'font-size:$this->fontsize; color:$this->color;'>$this->body</p>
                    </body>
                </html>";
           }
    }

?>


